---
title: "Code of Conduct"
---

Arch Linux Code of Conduct
==========================

The Arch community is a technical community whose shared purpose is to support and enhance Arch
Linux.

Arch Linux is a community-driven distribution; the developers, support staff and people who
provide assistance in the various fora all do so in their own time, motivated by a shared desire
to provide a minimal base system that can be configured by an individual to suit their specific
requirements. The code of conduct here has been developed over a number of years and reflects the
community's ethos of a functional support system with a high signal-to-noise ratio and an explicit
expectation of self-sufficiency, willingness to learn and contribution.

Familiarising yourself with the principles and guidelines here is both a courtesy to the community
and an effective way of making your initial interactions with other Arch Linux users mutually
beneficial.

The rules laid out in this document are necessary limitations to create a community that is useful
and welcoming to everybody. They allow members to satisfy all contributive impulses without
aggravating themselves or encroaching on the freedom of others. Embracing these principles and
obeying the guidelines therefore benefits the entire community by providing freedom from the
disorder and other oppressive, harmful and negative consequences of a more chaotic approach.

## Common sense introduction

- If you choose to use the Arch Linux distribution, you are welcomed, and encouraged to embrace the
  [Arch Linux principles](https://wiki.archlinux.org/title/Arch_Linux#Principles).
- When asking for help, read the manual, do your research and provide details for those you are
  asking for assistance.
- When offering help, be as patient and tactful as possible.

Arch is a community of volunteers. Rather than providing a complicated pattern of balanced freedoms
and restrictions, as well as the time, resources and personnel required to interpret and administer
such rules, we have chosen a simple principle to embrace for the benefit of all. Therefore, this
entire document may be condensed into one simple admonition:

___Proactively seek to give of yourself and to bring only benefit to your peers and community.___

## Code of conduct

The _minimum necessary standards_ when interacting with others in the Arch Linux community comprise
the following points.

### Respect

#### Respect other users

Arch Linux is a respectful, inclusive community. Anti-social or offensive behaviour will not be
tolerated. Simply put, treat others as you would be treated; respect them and their views, even if
you disagree with them. When you do find yourself disagreeing; counter the idea or the argument,
rather than engage in _ad hominem_ attacks.

#### Respect other operating systems and projects

Maligning other FOSS projects or distributions, or any other operating systems and their users is
prohibited. The entire Arch team is happy to volunteer their time and energy to provide you with
the Arch Linux distribution, documentation and forums. Kindly show respect toward the volunteers,
users and communities of other projects, distributions and operating systems as well. Views,
experiences and opinions are always welcome, but unproductive slander is not.

#### Respect the staff

[Support staff](https://archlinux.org/people/support-staff/) have been chosen for their ability
to exercise consistently good judgement and shall have the final say. Note that Arch Linux is not
run as a democracy. The staff shall always attempt to implement universally peaceful solutions, but
in the end, are charged with the responsibility of maintaining peaceful, civil order for the
majority of the community. Therefore, they cannot always please everyone with the decisions made.
Do your part to contribute to a healthy community and environment.

#### No trolling

A "troll" is a person who misuses their forum freedoms to intentionally disrupt, cause controversy,
incite an argument, and/or receive negative attention by deliberately posting provocative content.
The term may also be used as a verb, to refer to the act of posting such content, or as a noun, to
refer the content itself.

Trolls can be deceitful and frequently use indirect expressions of hostility through ambivalence
and implicit messages as a method of covertly insulting, intimidating, or inciting a person or
persons for their own sadistic pleasure. They often pick their words very carefully and are
therefore able to defend their masked attempts at creating unrest, redirecting the blame onto the
community and its supposed failure to understand them properly. Trolling is prohibited.

#### Do not flame

Flaming, in the most common sense definition, is directing negative, disrespectful, and/or
insulting comments toward someone. An equally or more negative response, resulting in a cycling
exchange of insults is often the consequence. Flaming fellow members (including the Arch team) will
not be tolerated. Avoid personal insults and sarcastic or patronizing language. _Discussions can
be productive, but quarreling is always destructive._

#### Be responsible

If an interpersonal issue arises, be open-minded to the possibility that your behaviour or intent,
actual or perceived, may have contributed to the problem. Arch Linux users are encouraged to
cultivate self awareness and remain peaceable toward their peers. Taking responsibility for our
actions is often a good first step toward a peaceful reconciliation.

### Examples of unwanted behaviour

From the guiding principles outlined above, it follows that the following
points are a (by far non-exclusive) list of forbidden behaviour.

#### Criminal solicitation

[Criminal solicitation](https://definitions.uslegal.com/c/criminal-solicitation/) is forbidden in
this community. In this context, "criminal solicitation" shall mean, "To actively or implicitly
facilitate, incite, move, or persuade others to some act of lawlessness or illegal activity."

Therefore do not post discussions which demonstrate, or link to, criminal solicitation in any form.
This includes, but is not limited to information or links to facilitate illegal drug use, theft,
network intrusion, creation of code for malicious purposes, prohibited software copying, prohibited
use of copyrighted/patented material, so-called "warez", or sites which provide torrents or links
to such content. Illegal content shall be removed swiftly and dealt with in full accordance with
known applicable law.

#### Spam/Advertising/Solicitation

Spamming is forbidden. Please alert staff to the presence of spam should you encounter it.
Offending spam accounts and associated IP addresses will be banned. Do not reply to spam posts as
it increases the amount of work required to clean them up.

Publicity, if it is related to Arch (as a project or community) or GNU/Linux/FOSS, will usually be
allowed. Promoting web-invites, blog posts or commercial promotions are actively discouraged, or
outright prohibited. Registering just to promote your issue/cause, FOSS-related or not, treats the
community as a resource and is not acceptable; if unsure about the appropriateness of your content,
contact the support staff before posting. Also be aware that posting shortened/obscured links is a
technique of spammers and deliberately inhibits the communities ability to judge the propriety of
the link. Therefore, only post links that include a clear destination.

#### Sockpuppetry

Limit your fora membership to one account only. Having multiple accounts is an unnecessary use of
resources. Further, it may be interpreted as trolling behaviour.

Any form of [sockpuppetry](https://en.wikipedia.org/wiki/Sockpuppet_(Internet)) or impersonation is
prohibited.

#### Personal topics/rants

Rants and complaints are actively discouraged. This type of content is much better suited to a blog
or other personal web space and is considered undesirable in the Arch community. Your contributions
should be open, productive and inviting to all participants. Also see
[Respect other operating systems and projects](#respect-other-operating-systems-and-projects).

#### Controversy/controversial topics

There is no explicit list of topics considered to be "trollish", controversial or provocative, but
in the past, posts pertaining to __Religion, Sports, Race, Nationalism__ and __Politics__ have
invariably been closed. Therefore, ___specifically avoid these and all divisive topics in the Arch
community.___ The staff certainly realize that such issues are deeply ingrained human realities.
However, this is a technical community and is not intended nor able to effectively facilitate such
commentary nor the resulting unrest.

#### Ineffective discussion ("bikeshed")

Discussions stating the equivalent of "there is a problem with the Arch system and methodology, we
need to discuss it" (sometimes referred to as ["bikeshedding"](
https://docs.freebsd.org/en/articles/mailing-list-faq/#bikeshed)) have been repeatedly proven
ineffective and inflammatory and will usually be closed down after a warning from the support
staff. Arch is a Do It Yourself community. If you have identified a systemic issue, find a solution
that works for you, implement it, then post.

Furthermore, questioning or discussing the methods used by the Arch Linux development team will be
monitored closely and locked or removed _if deemed unhelpful and/or unproductive_. Harsh,
unproductive criticism is also uncalled for.

If you have a question regarding Arch development, ensure that your topic poses a specific question
and be open-minded to responses. If possible, provide a solution or partial solution. Submitting
code and patches for discussion is always more pragmatic than asking others to do it for you.

#### Overly broad questions and help vampirism

The Arch Linux community values technical correctness. When seeking or giving help, remember to
strive for accuracy, completeness and correctness. An excellent introduction to the expectations of
the Arch community is ESR's [How To Ask Questions The Smart Way](
http://www.catb.org/~esr/faqs/smart-questions.html).

Do not be a ["help vampire"](https://slash7.com/2006/12/22/vampires/).

### Arch Linux distribution support ONLY

Arch-based distributions have their own support fora and users of those distributions should be
actively encouraged to seek support there. These distributions often use different packages,
package versions, repositories, or make custom system configurations silently, practically
rendering support for such projects within Arch Linux impossible. Community technical support shall
only be provided for the Arch Linux distribution and the Arch User Repository. Posting issues with,
and requesting support for, derivative distributions or operating systems other than Arch Linux are
prohibited.

## Enforcement

Behaviour running afoul of the guidelines layed out in this document should be reported in private
to the moderators of the respective forum first, see the following section for the appropriate
points of contact. If this is not possible, e.g. when the behaviour directly involves Arch Linux
staff, the Arch Linux project leader should be contacted instead.

All reports will be treated confidentially in order to respect the privacy of all parties involved.
If deemed necessary and appropriate, other Arch Linux staff members can be involved in the
discussion, but must uphold the same principles of confidentiality.

The responsible body will take the report of the incident under consideration and decide on the
appropriate reaction. All reports will be taken seriously and be processed in a timely manner. The
response is determined on a case by case basis. Possible actions can be, but are not limited to:
issue a warning, temporarily or permanently suspending or deleting the user account, or in extreme
cases, a ban from all Arch Linux projects.

As a general rule, for first-time offences a warning is usually the appropriate reaction, unless the
occurrence is judged to be especially flagrant, in which case a ban may be issued immediately. Such
a warning will usually be communicated in private to the user and needs to be acknowledged in
writing in order to make sure that the user has read and understood the scope of their
transgression. If the warning goes unheeded, further action will be taken, commonly resulting in a
temporary or permanent suspension.

All actions taken, such as warnings or suspensions, will be communicated to the affected user(s) in
private. If a user feels treated unjustly by such a decision, they may appeal to the appropriate
channel as described in the next section. If possible, the appeal should be handled by a different
staff member this time, e.g. by a different moderator, or the project leader if necessary.

## Contacting the staff

If you feel that an egregious oversight has been made, or if you need to alert the staff of any
abusive behaviour, there are a number of ways you can ask for assistance or redress. These include:

- For the Forums: use the "Report" button found below every text box, or email
  [forum@archlinux.org](mailto:forum@archlinux.org) to contact the forum admins.
- For the Wiki: contact the [ArchWiki:Maintenance Team](
  https://wiki.archlinux.org/title/ArchWiki:Maintenance_Team).
- For the IRC channels: contact [one of the ops](
  https://wiki.archlinux.org/title/Arch_IRC_channels#Channel_operators).
- For the Arch User Repository: contact [one of the Package Maintainers](
  https://archlinux.org/people/package-maintainers/) or write an email to
  [aur-general@lists.archlinux.org](mailto:aur-general@lists.archlinux.org).

## Further reading

In addition to the above points, ___there are [general
guidelines](https://wiki.archlinux.org/title/General_guidelines) outside of the terms of service___
that are specific to each of the community fora and contain useful information on how to conduct
oneself on the respective communication channel.

All users agree to review the current guidelines ___before___ any use of any of the communication
channels.

Changes to links in the _"Further reading"_ section and the contents of the respective guidelines
may occur outside of the 30 days notice period for changes to the Code of Conduct itself.

## About this document

This code of conduct has been originally developed on the Arch wiki, where [its history](
https://wiki.archlinux.org/index.php?oldid=648541&date-range-to=2021-01-09&action=history) can be
found. Later revisions of this document can be found in the [Arch Linux Service Agreements](
https://gitlab.archlinux.org/archlinux/service-agreements) repository.

The text is available under the terms of the [GNU Free Documentation License 1.3 or later](
https://www.gnu.org/licenses/fdl-1.3.html).
